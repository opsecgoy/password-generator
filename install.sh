#!/bin/sh
#
#  This script will add the genpw function to your ~/.bashrc file
#  which will allow you to enter randpw <n> into the terminal to 
#  generate a new password. 
#  
#  Where 'n' is a number specifying the length of the password.
#  If omitted, the default length is 16
#
#  Example: genpw 32 
#   Output: PuiSh4fKSP3pn02SlbNJqaUkdRiuvckt
#
# Make sure this thing isn't already appended to the ~/.bashrc file
if grep -Fq genpw ~/.bashrc
then
    # code if found
    echo "Script already installed"
    exit 0
else
    # code if not found
    echo "Installing script..."
    echo "genpw(){ < /dev/urandom tr -dc _A-Z-a-z-0-9 | head -c\${1:-16};echo;}" >> ~/.bashrc
fi

# Wait for a second then check if the line exists in the .bashrc file
sleep 0.3s
if grep -Fq genpw ~/.bashrc
then
    echo "Installation complete!"
    exit
else
    echo "Installation could not complete successfully\n\nPlease make sure you have permission to edit the ~/.bashrc file and try again.\n\nYou may have to run using sudo"
    exit
fi