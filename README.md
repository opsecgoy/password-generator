# Password Generator

Installs a script that allows you to generate a password through the terminal on most linux machines.

**Installation**
1. Download the install.sh script.
2. Add the 'execute' permission to the file.
    `chmod +x install.sh`
3. Run the install script.
    `./install.sh`
4. You're done! Now you can run `genpass N` to generate a random password in the terminal where "N" is a number specifying the length of the password. If omitted the default length is 16.